﻿using System;
namespace SimpleBlockchain
{
    public class ResolveResponse
    {
        public ResolveResponse()
        {
        }

        public string Message { get; set; }

        public IBlockchain Chain { get; set; }
    }
}
