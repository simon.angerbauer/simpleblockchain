﻿using System;
namespace SimpleBlockchain
{
    public class BlockchainTransaction
    {
        public BlockchainTransaction()
        {
        }

        public string Sender { get; set; }

        public string Recipient { get; set; }

        public double Amount { get; set; }
    }
}
