﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SimpleBlockchain
{
    public interface IBlockchain
    {
        public Block LastBlock { get; }

        public IEnumerable<string> Nodes { get; }

        public IEnumerable<Block> Blocks { get; }

        public int NewTransaction(string sender, string recipient, double amount);

        public int NewTransaction(BlockchainTransaction transaction);

        public Block NewBlock(long proof, byte[] previousHash = null);

        public long ProofOfWork(long lastProof);

        void RegisterNode(string node);

        Task<bool> ResolveConflicts();
    }
}
