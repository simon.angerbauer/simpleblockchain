﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SimpleBlockchain
{
    public class Blockchain : IBlockchain
    {
        private static HashAlgorithm HashAlgorithm = SHA256.Create();
        private static int Difficulty = 3;

        private List<BlockchainTransaction> _currentTransactions;
        private SortedList<int, Block> _chain;
        private HashSet<string> _nodes;

        public Block LastBlock => _chain.Values.Last();
        public IEnumerable<Block> Blocks => _chain.Values;

        public IEnumerable<string> Nodes => _nodes;

        public Blockchain()
        {
            _nodes = new HashSet<string>();
            _chain = new SortedList<int, Block>();
            _currentTransactions = new List<BlockchainTransaction>();

            CreateGenesisBlock();
        }

        public static byte[] Hash(Block block)
        {
            var blockJson = block.ToJson();
            var blockEncoded = Encode(blockJson);

            var hash = HashAlgorithm.ComputeHash(blockEncoded);
            return hash;
        }

        public static bool ValidProof(long lastProof, long proof)
        {
            var guess = $"{lastProof}{proof}";
            var guessEncoded = Encode(guess);
            var guessHash = HashAlgorithm.ComputeHash(guessEncoded);

            var isValid = guessHash.Take(Difficulty).All(b => b == 0);
            return isValid;
        }

        public static byte[] Encode(string value)
        {
            return Encoding.UTF8.GetBytes(value);
        }

        public int NewTransaction(string sender, string recipient, double amount)
        {
            var transaction = new BlockchainTransaction
            {
                Sender = sender,
                Recipient = recipient,
                Amount = amount
            };

            return NewTransaction(transaction);
        }

        public int NewTransaction(BlockchainTransaction transaction)
        {
            _currentTransactions.Add(transaction);

            var blockContainingThisTransaction = LastBlock.Index + 1;
            return blockContainingThisTransaction;
        }

        public Block NewBlock(long proof, byte[] previousHash = null)
        {
            var block = new Block
            {
                Index = _chain.Count + 1,
                Timestamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(),
                Transactions = _currentTransactions,
                Proof = proof,
                PreviousHash = previousHash ?? Hash(LastBlock)
            };

            _currentTransactions = new List<BlockchainTransaction>();
            _chain.Add(block.Index, block);

            return block;
        }

        public long ProofOfWork(long lastProof)
        {
            long proof = 0;
            while(!ValidProof(lastProof, proof))
            {
                proof += 1;
            }

            return proof;
        }

        public void RegisterNode(string node)
        {
            _nodes.Add(node);
        }

        public async Task<bool> ResolveConflicts()
        {
            var neighbours = _nodes;
            var maxLength = _chain.Count();
            IEnumerable<Block> newChain = null;
            var httpClient = new HttpClient();

            foreach (var node in neighbours)
            {
                var response = await httpClient.GetAsync($"{node}/simpleblockchain/chain");
                var chain = await response.Content.ReadAsAsync<IEnumerable<Block>>();

                if (chain.Count() > maxLength && IsValid(chain))
                {
                    newChain = chain;
                    maxLength = chain.Count();
                }
            }

            if(newChain != null)
            {
                _chain = new SortedList<int, Block>(newChain.ToDictionary(b => b.Index));
                return true;
            }

            return false;
        }

        private bool IsValid(IEnumerable<Block> chain)
        {
            var lastBlock = chain.First();

            foreach (var block in chain.Skip(1))
            {
                if (!PreviousHashValid(lastBlock, block) || !ValidProof(lastBlock.Proof, block.Proof))
                {
                    return false;
                }

                lastBlock = block;
            }

            return true;
        }

        private void CreateGenesisBlock()
        {
            NewBlock(100, new byte[] { 1 });
        }

        private bool PreviousHashValid(Block lastBlock, Block block)
        {
            var previousHashValid = block.PreviousHash.SequenceEqual(Hash(lastBlock));
            return previousHashValid;
        }
    }
}
