﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SimpleBlockchain
{
    public class Block
    {
        public Block()
        {
        }

        [JsonProperty(Order = 1)]
        public int Index { get; set; }

        [JsonProperty(Order = 2)]
        public long Timestamp { get; set; }

        [JsonProperty(Order = 3)]
        public List<BlockchainTransaction> Transactions { get; set; }

        [JsonProperty(Order = 4)]
        public long Proof { get; set; }

        [JsonProperty(Order = 5)]
        public byte[] PreviousHash { get; set; }

        public string ToJson()
        {
            var json = JsonConvert.SerializeObject(this);
            return json;
        }
    }
}
