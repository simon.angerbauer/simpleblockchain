﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace SimpleBlockchain.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SimpleBlockchainController : ControllerBase
    {
        private static string NodeIdentifier = Guid.NewGuid().ToString();

        private readonly ILogger<SimpleBlockchainController> _logger;
        private readonly IBlockchain _blockchain;

        public SimpleBlockchainController(ILogger<SimpleBlockchainController> logger, IBlockchain blockChain)
        {
            _logger = logger;
            _blockchain = blockChain;
        }

        [HttpGet]
        [Route("mine")]
        public IActionResult Mine()
        {
            var lastBlock = _blockchain.LastBlock;
            var lastProof = lastBlock.Proof;

            var proof = _blockchain.ProofOfWork(lastProof);
            var sender = "0";
            var recipient = NodeIdentifier;
            var amount = 1;
            _blockchain.NewTransaction(sender, recipient, amount);

            var previousHash = Blockchain.Hash(lastBlock);
            var block = _blockchain.NewBlock(proof, previousHash);

            return Ok(block);
        }

        [HttpPost]
        [Route("transactions/new")]
        public IActionResult PostNewTransaction([FromBody]BlockchainTransaction transaction)
        {
            if(string.IsNullOrEmpty(transaction.Sender) || string.IsNullOrEmpty(transaction.Recipient) || transaction.Amount <= 0)
            {
                return BadRequest("Missing Parameters");
            }

            var blockContainingThisTransaction = _blockchain.NewTransaction(transaction);
            return Ok($"Transaction will be added to Block {blockContainingThisTransaction}");
        }

        [HttpGet]
        [Route("chain")]
        public IActionResult GetChain()
        {
            var blocks = _blockchain.Blocks;
            return Ok(blocks);
        }

        [HttpPost]
        [Route("nodes/register")]
        public IActionResult RegisterNodes([FromBody]IEnumerable<string> nodes)
        {
            if(!nodes.Any())
            {
                return BadRequest("No nodes specified");
            }

            foreach (var node in nodes)
            {
                _blockchain.RegisterNode(node);
            }

            return Ok(_blockchain.Nodes);
        }

        [HttpGet]
        [Route("nodes/resolve")]
        public async Task<IActionResult> ResolveNodes()
        {
            var replaced = await _blockchain.ResolveConflicts();

            var message = replaced ? "Our chain was replaced" : "Our chain is authoritative";

            var response = new ResolveResponse
            {
                Message = message,
                Chain = _blockchain
            };

            return Ok(response);
        }
    }
}
